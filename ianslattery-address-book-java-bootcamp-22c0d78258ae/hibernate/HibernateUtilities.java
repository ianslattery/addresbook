package hibernate;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtilities {

	private static SessionFactory sessionFactory;
	private static ServiceRegistry serviceRegistry;
	
	static
	{
		try
		{	serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
			
			sessionFactory = new Configuration().buildSessionFactory(serviceRegistry);
		}
		catch(HibernateException exception){
			System.out.println("problem creating session factory");
		}
	}
	public static SessionFactory getSessionFactory(){
		return sessionFactory;
	}
}
