package app.dao;

import java.util.List;

import main.AddressBookEntry;

public interface AddressBookEntryDAO {		
	//Return Type					Method							Parameters
	String							createAddressBookEntry			(AddressBookEntry abe);
	AddressBookEntry 				getAddressBookEntry				(String emailAddress);
	List<AddressBookEntry> 			getAllAddressBookEntries		();
	String 							updateAddressBookEntry			(AddressBookEntry changedAbe);
	void 							deleteAddressBookEntry			(AddressBookEntry deletionAbe);
}